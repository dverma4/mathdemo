/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform,StyleSheet, Text, View, TextInput, ScrollView, RefreshControl} from 'react-native';
import questionData from './questionData.json';


export default class App extends Component {
  constructor(props) {
        super(props);
        this.state = {        
            count: 0,
            refreshing: false
        };
    }


  _onRefresh = () => {
    this.state.count < 3 ?  this.setState({ count: this.state.count+1, refreshing: false }) :  this.setState({ count: 0, refreshing: false })       
    }
     
    

  render() {

    const { questions, demo } = questionData;
    console.log(questionData)
    var num = this.state.count  

    const textInputComponents = questions[num].options.map((val,i)=> <Text key={i}> {i}) {val.value.replace(/<(?:.|\n)*?>/gm, '')} {'\n'} </Text>)
    return (
       <ScrollView 
          keyboardShouldPersistTaps="always"
          refreshControl={
                        <RefreshControl
                          refreshing={this.state.refreshing}
                          onRefresh={this._onRefresh}
                          tintColor='#ffffff'
                        />
                      }
      >

      <View style={styles.container}>
        <View>
          <Text style={styles.headingStyle}> Question :</Text>
          <Text style={styles.textStyle}>  {questions[num].question.replace(/<(?:.|\n)*?>/gm, '')} </Text>
        </View>
        <View style={{marginTop: 20}}>
         <Text style={styles.headingStyle}> Options : </Text>
          <Text style={styles.textStyle}>  {textInputComponents} </Text>
        </View>

         <View style={{marginTop: 20}}>
         <Text style={styles.headingStyle}> Demo : </Text>
          <Text style={styles.textStyle}>   {demo.replace(/<(?:.|\n)*?>/gm, '')} </Text>
        </View>

      </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
  textStyle:{
    fontSize: 20,
    textAlign:'center',
    justifyContent: 'center',
    alignItems: 'center',
    margin: 10
  },
  headingStyle:{
    fontSize: 22,
    fontWeight: 'bold',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20
  }
});
