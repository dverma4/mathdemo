# mathDemo
react-native application for rendering math equations

## Installation:
- **1)** *Clone the project*
- **2)** *cd to project folder*
- **3)** *run npm install*

### Screenshots:

![alt text](https://gitlab.com/dverma4/mathdemo/blob/master/screenshots/Screenshot_1539372179.png)
![alt text](https://gitlab.com/dverma4/mathdemo/blob/master/screenshots/Screenshot_1539372188.png)
